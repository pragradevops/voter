package co.pragra.javafullstack.voterapp.controller;


import co.pragra.javafullstack.voterapp.model.PollDAO;
import co.pragra.javafullstack.voterapp.service.PollService;
import com.pragra.javafullstack.voterapp.resources.models.ErrorMessage;
import com.pragra.javafullstack.voterapp.resources.models.Poll;
import com.pragra.javafullstack.voterapp.resources.models.Poll;
import com.pragra.javafullstack.voterapp.resources.rest.interfaces.PollsApi;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PollController implements PollsApi {

    private PollService service;

    public PollController(PollService service) {
        this.service = service;
    }


    @Override
    public ResponseEntity<Poll> getPollById(Long id) {
        int Id = id.intValue();
        return ResponseEntity.ok(service.getPollById(Id));
    }


    @Override
    public ResponseEntity<List<Poll>> getPolls() {
        return ResponseEntity.ok(service.getPoll()) ;
    }

    @Override
    public ResponseEntity<Poll> getPollById(Long id) {
        //int Id = id.intValue();
        return ResponseEntity.ok(service.getPollById(id));
    }

    @Override
    public ResponseEntity<Void> pollsDelete() {
        return ResponseEntity.status(400).build();
    }

    @Override
    public ResponseEntity<Void> addPoll(Poll poll) {
        service.addPoll(poll);
        return ResponseEntity.status(200).build();
    }

    @Override
    public ResponseEntity<List<Poll>> deletePoll(Long id) {
        int Id = id.intValue();
        service.deletePoll(Id);
        return getPolls(); //ResponseEntity.ok(service.getPolls());
    }

    @Override
    public ResponseEntity<Void> pollsIdPost(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<Void> pollsPost() {
        return null;
    }

    @Override
    public ResponseEntity<Object> updatePoll(Long id, Poll poll) {
        //int Id = id.intValue();
        service.updatePoll(id, poll);
        return ResponseEntity.ok(service.getPollById(id));
        int Id = id.intValue();
        service.updatePoll(Id, poll);
        return ResponseEntity.ok(service.getPollById(Id));

    }
}
