package co.pragra.javafullstack.voterapp;

import co.pragra.javafullstack.voterapp.service.PollService;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.pragra.javafullstack.voterapp.resources.models.Poll;

@SpringBootApplication
public class VoterappApplication {

	private PollService pollService;

	public VoterappApplication(PollService pollService) {
		this.pollService = pollService;
	}
	public static void main(String[] args) {
		ApplicationContext ctx= SpringApplication.run(VoterappApplication.class, args);
		System.out.println(ctx.getBean("contact", Contact.class));

	}

/*
	@Bean
	ApplicationRunner createPolls(){
		return args -> {

			this.pollService.savePoll(new Poll("C#"));
			//this.pollService.savePoll(new PollDAO("Angular"));

*/
/*
			pollService.addPoll(new Poll().id(1).title("Politics"));
			pollService.addPoll(new Poll().id(2).title("Science"));
			pollService.addPoll(new Poll().id(3).title("Technology"));
			pollService.addPoll(new Poll().id(4).title("Business"));
			pollService.addPoll(new Poll().id(5).title("Fashion"));
			pollService.addPoll(new Poll().id(6).title("Economics"));
*//*


		};
	}
*/


	@Bean
	ApplicationRunner  createPolls() {
		return args -> {
			pollService.addPoll(new Poll().id(1).title("Politics"));
			pollService.addPoll(new Poll().id(2).title("Science"));
			pollService.addPoll(new Poll().id(3).title("Technology"));
			pollService.addPoll(new Poll().id(4).title("Business"));
			pollService.addPoll(new Poll().id(5).title("Fashion"));
			pollService.addPoll(new Poll().id(6).title("Economics"));
		};
	}
}
