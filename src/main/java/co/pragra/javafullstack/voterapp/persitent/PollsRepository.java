package co.pragra.javafullstack.voterapp.persitent;

import co.pragra.javafullstack.voterapp.model.PollDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollsRepository extends JpaRepository<PollDAO, Long> {

    PollDAO findByTitle(String title);
}
