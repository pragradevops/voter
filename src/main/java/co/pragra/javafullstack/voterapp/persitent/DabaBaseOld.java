package co.pragra.javafullstack.voterapp.persitent;

import co.pragra.javafullstack.voterapp.model.Contact;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.StatementCallback;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.sql.*;

@Component
public class DabaBaseOld {

    private JdbcTemplate template;

    public DabaBaseOld(JdbcTemplate template) {
        this.template = template;
    }

    public void printData() {
        Contact contact = template.queryForObject("SELECT * FROM CONTACT", Contact.class);
        System.out.println(contact);
    }

}
