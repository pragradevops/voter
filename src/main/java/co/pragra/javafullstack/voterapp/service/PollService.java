package co.pragra.javafullstack.voterapp.service;


import co.pragra.javafullstack.voterapp.model.PollDAO;
import co.pragra.javafullstack.voterapp.persitent.PollsRepository;
import com.pragra.javafullstack.voterapp.resources.models.Poll;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PollService {
    List<Poll> polls;
    PollDAO pollDao = new PollDAO();

    private PollsRepository repository;

    public PollService(PollsRepository repository) {
        this.repository = repository;
    }

    public List<Poll> getPoll(){
        //List<PollDAO> polls = repository.findAll();
        //return polls;

        return this.repository.findAll().stream().map(pollDAO -> {
            Poll poll = new Poll();
            poll.setId(pollDAO.getId().intValue());
            poll.setTitle(pollDAO.getTitle());
            return poll;
       }).collect(Collectors.toList());
    }

    public void savePoll(Poll poll){
        if(this.repository.findByTitle(poll.getTitle()) == null){
            PollDAO pollDao = new PollDAO();
            pollDao.setTitle(poll.getTitle());
            this.repository.save(pollDao);
        }
    }

    //Poll poll = new Poll();

    public List<Poll> getPolls() {
        return polls;
    }

    public void setPolls(List<Poll> polls) {
        this.polls = polls;
    }

    public void addPoll(Poll poll){
        if(null ==polls){
            polls = new ArrayList<>();
            polls.add(poll);
        }
        else {
            polls.add(poll);
        }
    }

    public Poll getPollById(int id) {
        Poll thePoll = null;
        for(Poll poll : polls){
            if(poll.getId() == id){
                thePoll = poll;
                break;
            }
        }
        return thePoll;
    }

    public void deletePoll(int id) {
            for(Poll poll : polls){
                if(poll.getId() == id){
                    polls.remove(poll);
                    break;
                }
            }
         //polls.get(id).setTitle(null);
    }

    public Poll updatePoll(int id, Poll poll) {
        Poll updatedPoll = null;
            for(Poll thePoll : polls){
                if(thePoll.getId() == id){
                    thePoll.setTitle(poll.getTitle());
                    updatedPoll = thePoll;
                    break;
                }
            }
        return updatedPoll;

    }
}