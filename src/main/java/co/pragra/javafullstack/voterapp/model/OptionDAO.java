package co.pragra.javafullstack.voterapp.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "POLL_OPTIONS")
public class OptionDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "OPTION_NAME")
    private String name;

    @Column(name = "POLL_ID")
    private Long poll_id;

    @OneToMany(targetEntity = VoteDAO.class, mappedBy = "option_id")
    private List<VoteDAO> voteDAOS;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPoll_id() {
        return poll_id;
    }

    public void setPoll_id(Long poll_id) {
        this.poll_id = poll_id;
    }
}
