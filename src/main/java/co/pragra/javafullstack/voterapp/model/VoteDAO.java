package co.pragra.javafullstack.voterapp.model;

import javax.persistence.*;

@Entity
@Table(name = "OPTION_VOTES")
public class VoteDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "VOTER_NAME")
    private String voter_name;

    @Column(name = "OPTION_ID")
    private Long option_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVoter_name() {
        return voter_name;
    }

    public void setVoter_name(String voter_name) {
        this.voter_name = voter_name;
    }

    public Long getOption_id() {
        return option_id;
    }

    public void setOption_id(Long option_id) {
        this.option_id = option_id;
    }
}
