package co.pragra.javafullstack.voterapp.model;

import javax.persistence.*;
import java.util.List;

@Table(name = "POLLS")
@Entity
public class PollDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TITLE")
    private String title;


    @OneToMany(targetEntity = OptionDAO.class, mappedBy = "poll_id")
    private List<OptionDAO> optionDAOS;

    public PollDAO() {
    }

    public PollDAO(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<OptionDAO> getOptionDAOS() {
        return optionDAOS;
    }
}
