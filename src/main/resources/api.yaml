swagger: '2.0'
info:
  description: API for the mid-term API project/Voter application.
  version: '0.0.1'
  title: Voter Application APIs
basePath: /api
host: localhost:8080
tags:
  - name: Poll
paths:
  '/polls':
    get:
      summary: Get All the Polls
      description: Get All the Polls
      operationId: get-polls
      consumes:
        - application/json
      produces:
        - application/json
      responses:
        '200':
          description: The request was successful
          schema:
            type: array
            items:
              type: object
              $ref: '#/definitions/Poll'
        '404':
          description: No Polls found
          schema:
            $ref: '#/definitions/ErrorMessage'
        '500':
          description: The server encountered an unexpected condition which prevented it from fulfilling the request
          schema:
            $ref: '#/definitions/ErrorMessage'
    post:
      produces:
        - application/json
      consumes:
        - application/json
      summary: publish the message for particular topic
      description: Api will allow to publish the message for an topic
      responses:
        400:
          description: No Post allowed
          schema:
            $ref: '#/definitions/ErrorMessage'

    delete:
      summary: delete all the message for a topic from queue
      description: delete all the message for a topic from queue
      responses:
        400:
          description: No Delete allowed
          schema:
            $ref: '#/definitions/ErrorMessage'

  '/polls/{id}':
    get:
      summary: Get Poll by {Id}
      description: Get Poll by {Id}
      operationId: get-poll-by-id
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: id
          in: path
          description: ID of poll to fetch
          required: true
          type: integer
          format: int64

      responses:
        '200':
          description: The request was successful
          schema:
            type: object
            items:
              type: Poll
              $ref: '#/definitions/Poll'
        '404':
          description: No Poll found
          schema:
            $ref: '#/definitions/ErrorMessage'
        '500':
          description: The server encountered an unexpected condition which prevented it from fulfilling the request
          schema:
            $ref: '#/definitions/ErrorMessage'
    post:
      produces:
        - application/json
      consumes:
        - application/json
      parameters:
        - name: id
          in: path
          description: ID of poll to fetch
          required: true
          type: integer
          format: int64

      summary: publish the message for particular topic
      description: Api will allow to publish the message for an topic
      responses:
        400:
          description: No Post allowed
          schema:
            $ref: '#/definitions/ErrorMessage'
    put:
      summary: Update new poll
      description: Update poll
      operationId: update-poll
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - name: id
          in: path
          description: ID of poll to fetch
          required: true
          type: integer
          format: int64
        - in: body
          name: Poll
          description: The poll to update.
          schema:
            type: Poll
            required:
              - id
              - title
            properties:
              id:
                type: integer
              title:
                type: string

      responses:
        '201':
          description: Poll updated
          schema:
            type: object
            items:
              type: object
              $ref: '#/definitions/Poll'

    delete:
      summary: delete all the message for a topic from queue
      description: delete all the message for a topic from queue
      operationId: delete-poll
      parameters:
        - name: id
          in: path
          description: ID of poll to fetch
          required: true
          type: integer
          format: int64

      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              type: object
              $ref: '#/definitions/Poll'

  '/polls/poll':
    post:
      summary: Add a new poll
      description: Add a new poll
      operationId: add-poll
      consumes:
        - application/json
      parameters:
        - in: body
          name: Poll
          description: The user to create.
          schema:
            type: Poll
            required:
              - id
              - title
            properties:
              id:
                type: integer
              title:
                type: string

      responses:
        '201':
          description: Poll Created

definitions:
  Poll:
    type: object
    properties:
      id :
        type: integer
      title:
        type: string
        description: Title of the poll
      options:
        type: array
        items:
          $ref: '#/definitions/Options'

  Options:
    description: Option of the poll
    properties:
      id:
        type: integer
        description: ID of an option
      name:
        type: string
        description: name of the option displayed on the pol
      votes:
        type: array
        items:
          $ref: '#/definitions/Vote'

  Vote:
    description: Vote
    properties:
      id:
        type: integer
        description: ID of vote
      nameOftheVoter:
        type: string
        description: Name of the voter

  ErrorMessage:
    description: Returned for all error and bad request
    properties:
      code :
        type: integer
      message:
        type: string
        description: description of the error occured
